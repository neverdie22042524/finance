## New Department Checklist

#### Finance
* [ ]  Create a merge request with updated department [here](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/handbook/finance/department-structure/index.html.md)
* [ ]  Update NetSuite
* [ ]  Update TriNet

#### PeopleOps
* [ ]  BambooHR
* [ ]  Remove antiquated department (if applicable)
* [ ]  Add new department and which employees are moving to new department


#### Other departments
* [ ]  Update [org chart](https://about.gitlab.com/team/chart/) if applicable
* [ ]  Other handbook pages, if any

@artNasser, @wilsonlau, @chloe 