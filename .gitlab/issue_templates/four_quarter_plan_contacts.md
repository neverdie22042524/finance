## 4 Quarter Department Planning Process
This issue serves as a checklist to ensure that all tasks associated with **GitLab's Rolling 4 Quarter Forecast** is accounted for and accurate. 

## Send Out Invites
On the first Monday on the last month in the quarter, budget review invites need to be sent out to division leaders. Invites should be between the 17th and - 20th. 

* [ ] - Director of Support
* [ ] - VP of Product
* [ ] - Head of Product
* [ ] - CFO
* [ ] - CCO
* [ ] - VP of Engineering
* [ ] - CRO & VP of Sales
* [ ] - Director of Product Marketing
* [ ] - Senior Director, Marketing & Sales Development
* [ ] - Director of Demand Gen
* [ ] - Director of Corporate Marketing
* [ ] - Director of Business Development
* [ ] - Director of Data & Analytics

## Review Non Headcount & Headcount Budgets
During the budget reviews, division leaders should be expected to have an outline of the next rolling four quarters non-headcount & headcount expenses. 

- The non-headcount expense spreadsheet can be found [here](https://docs.google.com/spreadsheets/d/10YbNUgcpidnEIWlLjogh27p9cRhyWLvi0rdhOOwDQGo/edit#gid=452233162)
- Headcount expenses should be mocked up by the division leaders and discussed during the budget review. If a division head needs a template, he or she can refer to the template [here](https://docs.google.com/spreadsheets/d/13-S9q5DPwFhSUHMrQkqyTS0d-hYmb6cy87zw4Qt7dR0/edit?usp=sharing)

## Dates
* [ ] - 1st Monday on the last month in the quarter: Send Invites
* [ ] - 1st Tuesday on the last month in the quarter: Make 4 quarter forecast annoucement during team call
* [ ] - 14th - 17th of Month: Budget Reviews
* [ ] - 22nd of Month: Finalize Budgets with @pmachle
* [ ] - 22nd of Month: Take Snapshots of headcount & non-headcount expenses
* [ ] - 22nd of Month: Meet with @artNasser to load in new budgets to NetSuite

## Templated Invites

```
Hi, 

This is an invite and reminder to prepare for the upcoming rolling four quarter forecast review. 
During our meeting we will review your forecasted non-headcount and headcount expenses. 

All non-headcount expenses can be added and reviewed here: 
https://docs.google.com/spreadsheets/d/10YbNUgcpidnEIWlLjogh27p9cRhyWLvi0rdhOOwDQGo/edit#gid=452233162

If you need a template to project out your headcount expenses, you can use the templated here.
https://docs.google.com/spreadsheets/d/13-S9q5DPwFhSUHMrQkqyTS0d-hYmb6cy87zw4Qt7dR0/edit?usp=sharing. Please make your own copy

Let me know if you have any questions!
```

